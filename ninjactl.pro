DEFINES += QT_DEPRECATED_WARNINGS
TEMPLATE = subdirs
CONFIG += ordered

win32 {
    SUBDIRS = \
            $$PWD/lib/chat

    SUBDIRS += app
    app.depends = \
                chat

} else:android {
    message("Settings for android")
} else:macx {
    SUBDIRS = \
            $$PWD/lib/chat

    SUBDIRS += app
    app.depends = \
                chat

} else:ios {
    message("Settings for iOS")
} else {
    error("Unknown Config")
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
