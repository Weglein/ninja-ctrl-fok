LBNAME = lchat
TEMPLATE = lib
CONFIG += staticlib

QT  += \
    core \

INCLUDEPATH += $$PWD/../
DEPENDPATH  += $$PWD/../

win32 {
    !include($$PWD/../../win32_common.pri) {
        error("Couldn't find the win32_common.pri file!")
    }

    message("Building $${LBNAME} for Windows")
} else:android {

} else:macx {
     !include($$PWD/../../mac_common.pri) {
         error("Couldn't find the mac_common.pri file!")
     }

} else:ios {

} else {
    error("Unknown Config")
}

SOURCES += \
        base.cpp \
        participant.cpp
HEADERS += \
        base.hpp \
        participant.hpp
