#ifndef BASE_HPP
#define BASE_HPP

#include <QObject>
namespace scninja {

class Base : public QObject
{
    Q_OBJECT
public:
    explicit Base(QObject *parent = nullptr);

signals:

public slots:
};
}; // scninja
#endif // BASE_HPP
