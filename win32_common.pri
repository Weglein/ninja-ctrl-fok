CONFIG += c++17
DEFINES += BUILD_V=\\\"1.0.0\\\"
DEFINES += WIN32

INCLUDEPATH         += $$PWD/lib

DEPENDPATH          += $$PWD/lib

CONFIG(release, debug|release) {
    DESTDIR = release/bin
    OBJECTS_DIR = release/obj.d
    MOC_DIR = release/moc.d
}

CONFIG(debug, debug|release) {
    DESTDIR = debug/bin
    OBJECTS_DIR = debug/obj.d
    MOC_DIR = debug/moc.d
}
