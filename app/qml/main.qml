import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    id: mainWindow

    visible: true

    width: 600
    height: 600
    minimumHeight: 300
    minimumWidth: 333

    Loader {
        id: topBarLoader

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        // todo: use constant here
        height: 50

        source: "qrc:/qml/topBar/topBar.qml"
    }

    Component.onCompleted: {
        console.log("Done Preparing Window")
    }
}
