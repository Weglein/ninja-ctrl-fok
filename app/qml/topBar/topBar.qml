import QtQuick 2.0
import QtQuick.Controls 2.13


Item{
    id: topBarItem

    StackView {
        id: tbStackView

        anchors.fill: parent

        initialItem: tbMainRow
    }


    Component {
        id: tbMainRow

        Row {
            id: tbLeftCluster

            spacing: 15

            Button {
                id: tbChatButton
                text: "Chat"
            }

            Button {
                id: tbStatusButton
                text: "Status"
            }
        }
    }
}
