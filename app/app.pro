TARGET = NinjaCTL
TEMPLATE = app
CONFIG += c++17

INCLUDEPATH += $$PWD/../lib
DEPENDPATH  += $$PWD/../lib

SOURCES += main.cpp

win32 {
    DEFINES += WIN32
    
    RC_ICONS += $$PWD/../img/icons/ninja.ico

    QT  += \
        quick \
        core \
        qml \
        gui

    !include($$PWD/../win32_common.pri) {
        error("Couldn't find the win32_common.pri file!")
    }

    CONFIG(release, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/release/bin/ -lchat
    }

    CONFIG(debug, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/debug/bin/ -lchat
    }

    # Additional Libs
    # LIBS    += 

    RESOURCES += $$PWD/qml.qrc
    RESOURCES += $$PWD/qml-build.qrc
    
    DISTFILES =

} else:android {
    DEFINES += ANDROID
    
    # Set the proper manifest
#     contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
#         ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../android-sources/armeabi-v7a
#     }
# 
#     contains(ANDROID_TARGET_ARCH,arm64-v8a) {
#         ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../android-sources/arm64-v8a
#     }
# 
#     contains(ANDROID_TARGET_ARCH,x86) {
#         ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../android-sources/x86
#     }
# 
#     contains(ANDROID_TARGET_ARCH,x86_64) {
#        ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../android-sources/x86_64
#     }

    DISTFILES += \
#         $$PWD/../android-sources/AndroidManifest.xml \
#         $$PWD/../android-sources/arm64-v8a/AndroidManifest.xml \
#         $$PWD/../android-sources/armeabi-v7a/AndroidManifest.xml \
#         $$PWD/../android-sources/x86/AndroidManifest.xml \
#         $$PWD/../android-sources/x86_64/AndroidManifest.xml

    QT  += \
        quick \
        core \
        qml \
        svg

    !include($$PWD/../android_common.pri) {
        error("Couldn't find the android_common.pri file!")
    }

    CONFIG(release, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/release/bin/ -lchat
    }

    CONFIG(debug, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/debug/bin/ -lchat
    }

   # Additional Libs
    # LIBS    += 

    RESOURCES += $$PWD/qml.qrc
    RESOURCES += $$PWD/qml-build.qrc
    
} else:macx {
    DEFINES += MAC
    DEFINES += APPLE
    
    ICON += $$PWD/../img/icons/ninja.icns
    
    QT  += \
        quick \
        core \
        qml \
        gui
    
    !include($$PWD/../mac_common.pri) {
        error("Couldn't find the mac_common.pri file!")
    }
    
    CONFIG(release, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/release/bin/ -lchat
    }

    CONFIG(debug, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/debug/bin/ -lchat
    }
    
    # Additional Libs
    # LIBS    += 

    RESOURCES += $$PWD/qml.qrc
    RESOURCES += $$PWD/qml-build.qrc
    
    DISTFILES =
    
} else:ios {
    #NOTE: Apply patch file to prevent qt from squashing our configs for iOS
    QMAKE_IOS_DEPLOYMENT_TARGET=13.2
    CONFIG += no_bundle_defaults

    DEFINES += APPLE
    DEFINES += IOS

    QMAKE_TARGET_BUNDLE_PREFIX = # com.ninjarmm.ios
    QMAKE_INFO_PLIST = # $$PWD/../iOS-sources/Info.plist
    QMAKE_ASSET_CATALOGS += # $$PWD/../iOS-sources/Images.xcassets

    # Set launch screen
    #launchScreen.files = # $$PWD/../iOS-sources/NinjaLaunch.xib
    #launchScreen.files +=#  $$PWD/../img/NinjaLoading.png
    #QMAKE_BUNDLE_DATA += # launchScreen

     # Non Static linked frameworks
    # embededFrame.files = 
    # embededFrame.path = Frameworks
    # QMAKE_BUNDLE_DATA += embededFrame

    ios:!isEmpty(QMAKE_IOS_TARGETED_DEVICE_FAMILY) {
        warning("QMAKE_IOS_TARGETED_DEVICE_FAMILY is deprecated; use QMAKE_APPLE_TARGETED_DEVICE_FAMILY")
        QMAKE_APPLE_TARGETED_DEVICE_FAMILY = $$QMAKE_IOS_TARGETED_DEVICE_FAMILY
    }

    device_family.name = TARGETED_DEVICE_FAMILY
    device_family.value = $$QMAKE_APPLE_TARGETED_DEVICE_FAMILY
    QMAKE_MAC_XCODE_SETTINGS += device_family

    DISTFILES += \
        # $$PWD/../iOS-sources/Info.plist

    QT  += \
        quick \
        core \
        qml \
        svg

    !include($$PWD/../ios_common.pri) {
        error("Couldn't find the ios_common.pri file!")
    }

    CONFIG(release, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/release/bin/ -lchat
    }

    CONFIG(debug, debug|release) {
        LIBS            += -L$${OUT_PWD}/../lib/chat/debug/bin/ -lchat
    }

    # Additional Libs
    # LIBS    += 
    
    RESOURCES += $$PWD/qml.qrc
    RESOURCES += $$PWD/qml-build.qrc

    # TODO: Update to vcpkg or env var
#     QT_ROOT = /Users/sbrown/Qt/$${QT_VERSION}/ios
#     INCLUDEPATH += $$QT_ROOT/include
#     DEPENDPATH += $$QT_ROOT/include
    
} else {
    error("Unknown Config")
}
